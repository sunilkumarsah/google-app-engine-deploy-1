FROM google/cloud-sdk:242.0.0-alpine

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
