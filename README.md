# Bitbucket Pipelines Pipe: Google App Engine Deploy

Pipe to deploy an application to [Google App Engine][gae-deploy].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/google-app-engine-deploy:0.3.1
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    # DEPLOYABLES: '<string>' # Optional
    # VERSION: '<string>' # Optional.
    # BUCKET: '<string>' # Optional.
    # IMAGE: '<string>' # Optional.
    # PROMOTE: '<boolean>' # Optional
    # STOP_PREVIOUS_VERSION: '<boolean>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the app to deploy. |
| DEPLOYABLES                   |  List of white space separated yaml files to be passed to gcloud. Default `""`. If empty, `app.yaml` from the current directory will be used. |
| VERSION                       |  The version of the app to be created/replaced. |
| BUCKET                        |  The google cloud storage bucket to store the files associated with the deployment. |
| IMAGE                         |  Deploy with a specific GCR docker image. |
| PROMOTE                       |  If true the deployed version is receives all traffic, false to not receive traffic. |
| STOP_PREVIOUS_VERSION         |  If true the previous version receiving traffic is stopped, false to not stop the previous version. |
| EXTRA_ARGS                    |  Extra arguments to be passed to the CLI (see [Google app deploy docs][gae-deploy] for more details). |
| DEBUG                         |  Turn on extra debug information. Default `false`. |

_(*) = required variable._

More info about parameters and values can be found in the [Google app deploy docs][gae-deploy].

## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.3.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
```

### Advanced example: 
    
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.3.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      DEPLOYABLES: 'app-1.yaml app-2.yaml'
      VERSION: 'alpha'
      BUCKET: 'gs://my-bucket'
      IMAGE: 'gcr.io/my/image'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--logging=debug'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,google
[gae-deploy]: https://cloud.google.com/sdk/gcloud/reference/app/deploy
